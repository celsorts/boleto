# mIti - Microsserviço de Cadastro

## Endpoints

### POST /

*Cria um novo cadastro*

Exemplo body:
```
{
    "nome": "João da Silva",
    "cpfNumero": "12312312312",
    "cpfFotoUrl": "https://bucketmiti.com/foto",
}
```

### PATCH /{cpf}

*Efetiva um cadastro*
  
### GET /{cpf}

*Consulta um cadastro*
