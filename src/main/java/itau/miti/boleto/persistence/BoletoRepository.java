package itau.miti.boleto.persistence;

import org.springframework.data.repository.CrudRepository;

public interface BoletoRepository extends CrudRepository<Boleto, String>{
}