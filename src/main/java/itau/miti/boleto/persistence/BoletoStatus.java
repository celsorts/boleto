package itau.miti.boleto.persistence;

public enum BoletoStatus {
    PAGO,
    PENDENTE;
}