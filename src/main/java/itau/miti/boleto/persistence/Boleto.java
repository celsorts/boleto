package itau.miti.boleto.persistence;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Boleto{
    @Id
    private String codigoBarras;
    @NotBlank
    private String cpf;
    private double valor;
    @NotNull
    private BoletoStatus status;
    @NotNull
    private LocalDate dataExpiracao;
}