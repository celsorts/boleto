package itau.miti.boleto.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import itau.miti.boleto.application.auth.Usuario;
import itau.miti.boleto.application.dto.BoletoRequest;
import itau.miti.boleto.persistence.Boleto;
import itau.miti.boleto.services.BoletoService;


@RestController
public class BoletoController {
    @Autowired
    private BoletoService service;

    @PostMapping
    public Boleto emitir(@AuthenticationPrincipal Usuario usuario,
        @RequestBody BoletoRequest request){
        return service.criar(usuario.getCpf(), request.getValor());
    }

    @PatchMapping("/{codigoBarras}")
    public void pagar(@PathVariable String codigoBarras){
        service.pagar(codigoBarras);
    }
}
