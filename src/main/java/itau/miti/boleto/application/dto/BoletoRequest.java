package itau.miti.boleto.application.dto;

import lombok.Data;

@Data
public class BoletoRequest {
    private double valor;
}