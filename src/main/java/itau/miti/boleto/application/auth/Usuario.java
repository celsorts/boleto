package itau.miti.boleto.application.auth;

import lombok.Data;

@Data
public class Usuario {
    private String cpf;
}