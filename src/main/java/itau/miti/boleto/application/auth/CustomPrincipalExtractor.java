package itau.miti.boleto.application.auth;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

public class CustomPrincipalExtractor implements PrincipalExtractor {

  @Override
  public Object extractPrincipal(Map<String, Object> map) {
    for(String key : map.keySet()){
      System.out.println(key);
    }

    Usuario usuario = new Usuario();
    LinkedHashMap<String, Object> principal = (LinkedHashMap) map.get("principal");
    usuario.setCpf((String) principal.get("username"));
    
    return usuario;
  }
}
