package itau.miti.boleto.application;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ErrorHandler {
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public Map<String, String> tratarErroValidacao(ConstraintViolationException exception){
        Map<String, String> erros = new HashMap<>();

        for(ConstraintViolation violation : exception.getConstraintViolations()){
            erros.put(violation.getPropertyPath().toString(), violation.getMessage());
        }

        return erros;
    }
}
