package itau.miti.boleto.services;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import itau.miti.boleto.persistence.Boleto;
import itau.miti.boleto.persistence.BoletoRepository;
import itau.miti.boleto.persistence.BoletoStatus;

@Service
public class BoletoService{
    @Autowired
    private BoletoRepository repository;

    @Autowired
    private KafkaTemplate<String, Boleto> kafkaTemplate;

    public Boleto criar(String cpf, double valor){
        Boleto boleto = new Boleto();
        boleto.setCpf(cpf);
        boleto.setValor(valor);
        boleto.setDataExpiracao(LocalDate.now().plusDays(5));
        boleto.setStatus(BoletoStatus.PENDENTE);
        boleto.setCodigoBarras(gerarCodigoBarras());

        return repository.save(boleto);
    }

    private String gerarCodigoBarras(){
        String codigo = "";
        Random random = new Random();

        for(int i = 0; i < 4; i++){
            codigo += random.nextInt(9);
        }

        return codigo;
    }

    public Boleto pagar(String codigoBarras){
        Boleto boleto = carregarBoleto(codigoBarras);
        boleto.setStatus(BoletoStatus.PAGO);

        kafkaTemplate.send("boleto", boleto);

        return repository.save(boleto);
    }

    private Boleto carregarBoleto(String codigoBarras){
        Optional<Boleto> optional = repository.findById(codigoBarras);

        if(optional.isEmpty()){

        }

        return optional.get();
    }

    public Iterable<Boleto> listarTodos(){
        return repository.findAll();
    }
}